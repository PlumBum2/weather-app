import React, { useReducer, useEffect } from 'react';
import "./App.scss";
import Header from './components/header';
import Loader from 'react-loader-spinner';
import Details from './components/details';
import Calendar from './components/calendar';
import reducer, { initialState } from './state/reducer';
import { loadCurrentLocation, loadPlaces } from './state/actionCreators';
import { getSelectedForecastDetails, getCalendarData, getSelectedForecastPosition } from './state/selectors';
import { getFormatedDateRange } from './utils/dateTimeUtils';

const App = () => {
    const [state, dispatch] = useReducer(reducer, initialState);
    useEffect(() => {
        loadPlaces(dispatch);
        loadCurrentLocation(dispatch)
    }, [])
    const { initialLocation, forecastError } = state;
    const details = getSelectedForecastDetails(state);
    const selectedForecast = getSelectedForecastPosition(state);
    const calendarData = getCalendarData(state);
    const { places , isLoadingForecast, loadedDataLocation } = state;
    return <div className='app'>
        <Header/>
        <div className='app__content'>
            <div className='app__details'>
                <Details
                    places={places}
                    initialLocation={initialLocation}
                    forecast={details}
                    dispatch={dispatch}
                    loadedLocation={loadedDataLocation}
                />
            </div>
            <div className='app__calendar'>
                <span className={'app__date-text'}>{getFormatedDateRange()}</span>
                <Calendar
                    errorType={forecastError}
                    dispatch={dispatch}
                    selectedForecast={selectedForecast}
                    data={calendarData}
                />
            </div>
        </div>
        {
            isLoadingForecast && (
                <div className={'app__loader-container'}>
                    <Loader
                        type="TailSpin"
                        color='#5FA6F1'
                        height={100}
                        width={100}      
                    />
                </div>
            )
        }
    </div>
}

export default App;
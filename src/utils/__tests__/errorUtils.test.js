import { parseErrorType } from '../errorUtils';

describe('parseErrorType', () => {
    it('if no error response should return UNEXPECTED', () => {
        expect(parseErrorType({})).toEqual('UNEXPECTED');
    })

    it('if error response status is 404 should return NOT_FOUND', () => {
        expect(parseErrorType({response: {status: 404}})).toEqual('NOT_FOUND');
    })

    it('if error response status is unknow should return UNEXPECTED_SERVER', () => {
        expect(parseErrorType({response: {status: 500}})).toEqual('UNEXPECTED_SERVER');
    })
})
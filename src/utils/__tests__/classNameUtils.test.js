import { formatClass } from "../classNameUtils"

describe('formatClass', () => {
    it('should add class if predicate undefined', () => {
        expect(formatClass([['test-class']])).toEqual('test-class');
    })

    it('should concat classes with space between', () => {
        expect(formatClass([['test-class'],['second']])).toEqual('test-class second');
    })

    it('should add classes with true predicate', () => {
        expect(formatClass([['test-class'],['second', true]])).toEqual('test-class second');
    })

    it('should not add classes with false predicate', () => {
        expect(formatClass([['test-class'],['second', false]])).toEqual('test-class');
    })
})
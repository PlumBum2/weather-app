export const formatClass = (data) => 
    data.map(ci => {
        if(ci[1] === undefined) return ci[0];
        if(ci[1]) {
            return ci[0]
        }
        return null
    }).filter(c => !!c)
    .join(' ');


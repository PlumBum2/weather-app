import { format, addDays, setSeconds, setMinutes, setHours, addHours  } from 'date-fns'

export const getFormatedDateRange = () => {
    const now = new Date();
    return `${format(now, 'MMMM dd')}-${format(addDays(now, 7), 'dd')}`
}

export const formatAppTimeTable = (date) => {
    const startDay = setSeconds(setMinutes(setHours(date, 0), 0), 0);
    return [
        addHours(startDay, 6),
        addHours(startDay, 10),
        addHours(startDay, 14),
        addHours(startDay, 18),
        addHours(startDay, 22),
        addHours(startDay, 26),
    ]
}
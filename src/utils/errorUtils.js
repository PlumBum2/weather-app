const ERROR_TYPE_MESSAGE_MAP = {
    'NOT_FOUND': 'Forecast for this location not found',
    'UNEXPECTED_SERVER': 'Unexpected server error',
    'UNEXPECTED': 'Unexpected error'
}

export const parseErrorType = (e) => {
    if(e.response && e.response.status) {
        switch(e.response.status) {
            case 404: 
                return 'NOT_FOUND';
            default:
                return 'UNEXPECTED_SERVER'
        }
    }
    return 'UNEXPECTED'
}

export const getErrorMessage = (type) => ERROR_TYPE_MESSAGE_MAP[type];
import React from 'react';
import './WeatherIcon.scss';
import icons from '../../assets/weather-icons/*.svg'
import { formatClass } from '../../utils/classNameUtils';

const nightIcons = [
    'clear',
    'isolated-clouds',
    'scattered-clouds'
];

const getNightSuffix = (name, night) =>
 (night && nightIcons.includes(name))? '-night' : '';

const renderIcon = (name, night) => {
    try {
        const Icon = icons[name + getNightSuffix(name, night)].default;
        return <Icon/>;
    } catch (e) {
        return <span>NA</span>
    }
}

const WeatherIcon = (props) => {
    const { monochrome, code, night, small } = props;
    const c = formatClass([
        ['weather-icon'],
        ['weather-icon__monochrome', !!monochrome],
        ['weather-icon__small', !!small],
        ['weather-icon__large', !small]
    ]);
    return <div className={c} >
        {renderIcon(code, night)}
    </div>;
}


export default WeatherIcon;
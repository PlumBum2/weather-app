import React from 'react';
import icons from '../../../assets/parameter-icons/*.svg'
import './ParameterItem.scss';

const renderIcon = (code) => {
    try {
        const Icon = icons[code].default;
        return <Icon/>;
    } catch (e) {
        return null
    }
}

const ParameterItem = ({ iconCode, value, label, suffix}) => (
    <div className={'parameter-item'}>
        <div className={'parameter-item__icon-container'}>
            {renderIcon(iconCode)}
        </div>
        <span className={'parameter-item__text'}>{label} {value}{suffix}</span>
    </div>
)

export default ParameterItem;
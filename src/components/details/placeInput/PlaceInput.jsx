import React, { useState, useCallback, useEffect, useRef } from 'react';
import './PlaceInput.scss';
import SearchIcon from '../../../assets/other/search.svg';
import { formatClass } from '../../../utils/classNameUtils';


const PlaceInput = (props) => {
    const { initialValue, places, searchForecast } = props;
    const [ isFocused, setIsFocused ] = useState(false)
    const [ activeSuggestions, setActiveSuggestions ] = useState(places)
    const [ selectedItem, setSelectedItem ] = useState(-1)
    const input = useRef(null);

    const isSuggestionsAvailable = useCallback(() => {
        return activeSuggestions && activeSuggestions.length > 0;
    })

    const getValue = () => {
        if(input.current) {
            return input.current.value;
        }
        else return null;
    }
    const setValue = (value) => {
        if(input.current) {
            input.current.value = value;
        }
        filterActiveSugestions();
    }
    

    const filterActiveSugestions = useCallback(() => {
        if(places) {
            const filtered = places.filter(f => f.name.toLowerCase().startsWith(getValue().toLowerCase()))
            setActiveSuggestions(filtered.length > 9 ? filtered.slice(0, 8): filtered);
            setSelectedItem(-1);
        }
    })

    const onChange = useCallback((e) => {
        const value = e.target.value;
        filterActiveSugestions();
    })
    
    const blur = () => {
        if(input.current) {
            input.current.blur();
        }
    }


    useEffect(() => {
        filterActiveSugestions();
        if(initialValue !== getValue()) {
            setValue(initialValue)
        }
    }, [places, initialValue])

    const handleKeyDown = useCallback((e) => {
        if (e.keyCode === 38 && selectedItem > 0) {
            setSelectedItem(selectedItem - 1)
        } else if (e.keyCode === 40 && selectedItem < activeSuggestions.length - 1) {
            setSelectedItem(selectedItem + 1)
        } else if (e.keyCode === 13) {
            if(selectedItem > -1){
                onSelectItem(activeSuggestions[selectedItem].name)
            } else {
                onSelectItem(getValue());
            }
        }
      }
    )

    const onSelectItem = useCallback((p) => {
        setValue(p);
        blur();
    })

    const onBlur = useCallback(() => {
        const val = getValue();
        setIsFocused(false)
        const place = places && places.find(p => p.name.toLowerCase() === val.toLowerCase());
        if(place) {
            searchForecast(place.code);
        } else if(isSuggestionsAvailable()) {
            setValue(activeSuggestions[0].name)
            searchForecast(activeSuggestions[0].code);
        } else {
            searchForecast(val.toLowerCase());
        }
    });


    return (
        <div className={'place-input'}>
            <div className={'place-input__input-container'}>
                <input
                    ref={input}
                    onChange={onChange}
                    onKeyDown={handleKeyDown}
                    onFocus={() => setIsFocused(true)}
                    onBlur={onBlur}
                    className={'place-input__input'}
                />
                <SearchIcon className={'place-input__icon'}/>
            </div>
            {
                isFocused && isSuggestionsAvailable() && (
                    <div className={'place-input__places'}>
                        <div className={'place-input__places-container'}>
                            {   isSuggestionsAvailable() &&
                                activeSuggestions.map((p, i) => (
                                        <div
                                            onMouseDown={(e) => e.preventDefault()}
                                            onClick={(e) => onSelectItem(p.name)}
                                            className={formatClass([
                                                ['place-input__places-item'],
                                                ['place-input__item-selected', i === selectedItem]
                                            ])}
                                            key={p.code}
                                        >
                                            <span>{p.name}</span>
                                        </div>
                                    )
                                )
                            }
                        </div>
                    </div>
                )
            }
        </div>
    );
}

export default PlaceInput;
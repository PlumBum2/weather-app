import React, { useCallback } from 'react';
import './Details.scss';
import Logo from '../Logo';
import { loadForecast } from '../../state/actionCreators'
import WeatherIcon from '../weatherIcon';
import ParameterItem from './parameterItem';
import PlaceInput from './placeInput';


const Details = (props) => {
    const {loadedLocation, places, initialLocation, dispatch, forecast } = props;
    const getForecast = useCallback((place) => loadForecast(dispatch, place, loadedLocation));
    const renderForecast = useCallback((forecast) => {
        const {
            conditionCode,
            displayTime,
            airTemperature,
            windSpeed,
            windGust,
            windDirection,
            cloudCover,
            seaLevelPressure,
            totalPrecipitation,
            isNight
         } = forecast;
        return (     
            <div className={'details__content'}>
                <div className={'details__con-temp-container'}>
                    <WeatherIcon code={conditionCode} night={isNight} monochrome/>
                    <span className={'details__temp'}>{airTemperature}°</span><br/>
                </div>
                <span className={'details__cond-text'}>{displayTime} {conditionCode}</span>
                <div className={'details__param-cont'}>
                    <div>
                        <ParameterItem iconCode={'wind-speed'} label={'Wind speed: '} value={windSpeed} suffix={'ms'}/>
                        <ParameterItem iconCode={'wind-gust'} label={'Wind gust: '} value={windGust} suffix={'ms'}/>
                        <ParameterItem iconCode={'wind-direction'} label={'Wind direction: '} value={windDirection}/>
                    </div>
                    <div>
                        <ParameterItem iconCode={'cloud-cover'} label={'Cloud cover: '} value={cloudCover} suffix={'%'}/>
                        <ParameterItem iconCode={'sea-level-pressure'} label={'sea level pressure: '} value={seaLevelPressure}/>
                        <ParameterItem iconCode={'total-precipitation'} label={'Total precipitation: '} value={totalPrecipitation}/>
                    </div>
                </div>
            </div>
        );
    })
    return (
        <div className={'details__container'}>
            <div className={'details__logo'}>
                <Logo/>
            </div>
            <div className={'details__input'}>
                <PlaceInput
                    searchForecast={getForecast}
                    places={places}
                    initialValue={initialLocation || ''}
                />
            </div>
            {!!forecast && renderForecast(forecast)}
        </div>
    );
}


export default Details;
import React, { useState, useCallback } from 'react';
import CalendarItem from '../calendarItem';
import ArrowIcon from '../../../assets/other/arrow-down.svg';
import { formatClass } from '../../../utils/classNameUtils';
import './Row.scss';

const Row = (props) => {
    const { data, day, selectedForecast, onCellClick } = props;
    const isToday = () => day === 'Today';
    const [isCollapsed, setCollapse] = useState(!isToday)

    const toggleCollapse = useCallback(() => {
        setCollapse(!isCollapsed);
    })
    return (
        <div className={'row'}>
            <div
                onClick={toggleCollapse}
                className={formatClass([
                    ['row__day-column'],
                    ['row__day-hidden', isToday()]
            ])}>
                <span className={'row__day-text'}>{day}</span>
                <div
                    className={formatClass([
                        ['row__arrow-container'],
                        ['row__arrow-rotated', isCollapsed]
                    ])}
                >
                    <ArrowIcon/>
                </div>
            </div>
            <div
                className={formatClass([
                    ['row__cells-container'],
                    ['row__cells-hidden', isCollapsed]
                ])}
            >
            {
                data.map((d, i) => (
                    <div
                        onClick={() => onCellClick(i)}
                        key={d.displayTime}
                        className={'row__cell'}
                    >
                        <div className={'row__content-container'}>
                            <CalendarItem
                                    displayTime={d.displayTime}
                                    night={d.isNight}
                                    isPast={d.isPast && i !== selectedForecast}
                                    weatherIcon={d.conditionCode}
                                    temperature={d.airTemperature}
                                    windSpeed={d.windSpeed}
                                />
                        </div>
                        <div
                            className={
                                i === selectedForecast ?
                                'row__selected-marker':
                                'row__selected-placeholder'
                            }
                        />
                    </div>
                ))
            }
            </div>
        </div>
    );
}

export default Row;
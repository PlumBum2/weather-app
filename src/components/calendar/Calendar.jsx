import React, { useCallback } from 'react';
import './Calendar.scss';
import Row from './row';
import HeaderRow from './headerRow';
import { setSelectedForecast } from '../../state/actions'
import ErrorContainer from '../errorContainer';

const Calendar = (props) => {
    const { data, errorType, selectedForecast, dispatch } = props;
    const onCellSelect = useCallback((rowIndex, columnIndex) => {
        dispatch(setSelectedForecast(rowIndex, columnIndex));
    }) 
    return (
        <div className={'calendar__container'}>
                {
                    data ? (
                        <>
                        <HeaderRow times={data[0].rowData.map(d => d.displayTime)} />
                        {
                            data.map((d, i) => (
                                <Row
                                    key={d.day}
                                    selectedForecast={selectedForecast[0] === i ? selectedForecast[1] : null}
                                    day={d.day}
                                    data={d.rowData}
                                    onCellClick={(columnIndex) => onCellSelect(i, columnIndex)}
                                />
                            ))
                        }
                        </>
                    ):
                    <ErrorContainer type={errorType}/>
                }
        </div>
    );
}

export default Calendar;
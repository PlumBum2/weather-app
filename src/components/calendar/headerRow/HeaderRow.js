import React from 'react';
import './HeaderRow.scss';


const HeaderRow = ({ times }) => {
    return (
        <div className='header-row'>
            <div className={'header-row__day-column'}/>
            <div className={'header-row__cells-container'}>
            {
                times.map(t => (
                    <div key={t} className={'header-row__cell'}>
                        <span className={'header-row__text'}>{t}</span>
                    </div>
                ))
            }
            </div>
        </div>
    );
}

export default HeaderRow;
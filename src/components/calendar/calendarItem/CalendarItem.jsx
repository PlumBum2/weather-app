import React from 'react';
import WindDirectionIcon from '../../../assets/parameter-icons/wind-direction.svg';
import WeatherIcon from '../../weatherIcon';
import './CalendarItem.scss';

const CalendarItem = (props) => {
    const { weatherIcon, temperature, windSpeed, isPast, night, displayTime } = props;
    return (
        <div className={'calendar-item'} style={{opacity: isPast ? 0.4 : 1}}>
            <div className={'calendar-item__time-container'}>
                <span className={'calendar-item__time-text'}>{displayTime}</span>
            </div>
            <div className={'calendar-item__icon-container'}>
                <WeatherIcon night={night} small code={weatherIcon}/>
            </div>
            <span className={'calendar-item__temperature-text'}>{temperature}°</span><br/>
            <span className={'calendar-item__wind-speed-text'}>
                <WindDirectionIcon/><span>{windSpeed} ms</span>
            </span>
        </div>
    )
}


export default CalendarItem;
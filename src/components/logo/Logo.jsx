import React from 'react';
import './Logo.scss';

const Logo = () => <span className={'logo__text'}>weather</span>;

export default Logo;
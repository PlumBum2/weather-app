import React from 'react';
import './ErrorContainer.scss';
import { getErrorMessage } from '../../utils/errorUtils';

const ErrorContainer = ({ type }) => {
    return (
        <div className={'error__container'}>
            <span className={'error__text'}>{getErrorMessage(type)}</span>
        </div>
    );
}

export default ErrorContainer;
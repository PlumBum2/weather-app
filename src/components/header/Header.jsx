import React from 'react';
import './Header.scss';
import Logo from '../logo';

const Header = () => (
    <div className={'header__container'}>
        <Logo/>
    </div>
);

export default Header;

export const getSelectedForecastDetails = (state) => {
    const {
        calendarData,
        selectedForecast
    } = state;
    return calendarData ? calendarData[selectedForecast[0]].rowData[selectedForecast[1]] : null;
}

export const getSelectedForecastPosition = (state) => state.selectedForecast;

export const getCalendarData = (state) => state.calendarData
import axios from "axios";
//const PROXY_URL = 'https://crossorigin.me/';
const PROXY_URL = 'https://cors-anywhere.herokuapp.com/';

const BASE_URL = `${PROXY_URL}https://api.meteo.lt/v1`;
const api = axios.create({
    baseURL: BASE_URL,
    crossdomain: true,
    withCredentials: false,
  });

export const fetchPlaces = () => api.get('/places');

export const fetchCurentLocation = () => api.get(
    'http://ip-api.com/json',
    {baseURL: ''}
)

export const fetchForecast = (location) => api.get(`/places/${location}/forecasts/long-term`)
import { 
    closestIndexTo,
    parseISO,
    addDays,
    format,
    isToday,
    isBefore
 } from "date-fns";
import { fetchCurentLocation, fetchForecast, fetchPlaces } from "./api"
import {
    setLocation,
    setForecast,
    setPlaces,
    setSelectedForecast,
    setLoadingForecast,
    setForecastErrorType } from "./actions"
import { parseErrorType } from "../utils/errorUtils";
import { formatAppTimeTable } from "../utils/dateTimeUtils";

export const loadPlaces = async (dispatch) => {
    try {
        const { data } = await fetchPlaces()
        dispatch(setPlaces(data));

    } catch (e) {
        console.error(e)
    }
}

export const loadCurrentLocation = async (dispatch) => {
    try{
        const { data: { city } } = await fetchCurentLocation()
        dispatch(setLocation(city))
        const calendarData = await loadForecast(dispatch, city, null)
        selectInitialCell(dispatch, calendarData);
    }
    catch (e) {
        console.error(e)
    }
}

export const selectInitialCell = (dispatch, calendarData) => {
    const now = Date.now();
    const todayForecast = calendarData.find(d => d.day === 'Today').rowData;
    const columnIndex = closestIndexTo(now, todayForecast.map(f => f.forecastTimeUtc))
    dispatch(setSelectedForecast(0, columnIndex))
};

export const loadForecast = async (dispatch, location, prevLocation) => {
    const locationLower = location && location.toLowerCase();
    const prevLocationLower = prevLocation && prevLocation.toLowerCase();
    if(locationLower === prevLocationLower) {
        return;
    }
    try{
        dispatch(setLoadingForecast());
        const { data } = await fetchForecast(locationLower)
        const calendarData = formatCalendarData(data);
        dispatch(setForecast(calendarData, locationLower))
        return calendarData;
    }
    catch (e) {
        console.error(e);
        dispatch(setForecast(null, locationLower))
        dispatch(setForecastErrorType(parseErrorType(e)));
    }
}

export const formatCalendarData = (data) => {
    const { forecastTimestamps } = data;

    if(!forecastTimestamps) return [];
    const now = Date.now();
    const withParsedDates = forecastTimestamps.map(f => ({
        ...f,
        forecastTimeUtc: parseISO(f.forecastTimeUtc)
    }))

    return [
        formatDayForecasts(withParsedDates, now),
        formatDayForecasts(withParsedDates, addDays(now, 1)),
        formatDayForecasts(withParsedDates, addDays(now, 2)),
        formatDayForecasts(withParsedDates, addDays(now, 3)),
        formatDayForecasts(withParsedDates, addDays(now, 4)),
        formatDayForecasts(withParsedDates, addDays(now, 5)),
        formatDayForecasts(withParsedDates, addDays(now, 6)),
    ]
}

const formatDayForecasts = (forecastsTimestamps, date) => {
    const timeTable = formatAppTimeTable(date);
    const now = Date.now();
    const dates = forecastsTimestamps.map(f => f.forecastTimeUtc);
    const day = isToday(date) ? 'Today' : format(date, 'EEE');
    return {
        day: day,
        rowData: timeTable.map(t => ({
            isPast: isBefore(t, now),
            displayTime: format(t, 'HH:mm'),
            isNight: t.getHours() > 19 || t.getHours() < 5,
            ...forecastsTimestamps[closestIndexTo(t, dates)]
        }))
    }
}
export const SET_LOCATION = 'SET_LOCATION';
export const SET_FORECAST = 'SET_FORECAST';
export const SET_PLACES = 'SET_PLACES';
export const SET_SELECTED_FORECAST = 'SET_SELECTED_FORECAST';
export const SET_LOADING_FORECAST = 'SET_LOADING_FORECAST';
export const SET_FORECAST_ERROR_TYPE = 'SET_FORECAST_ERROR_TYPE';

export const initialState = {
    initialLocation: null, 
    calendarData: null,
    loadedDataLocation: null,
    places: null,
    isLoadingForecast: false,
    selectedForecast: [0,0],
    forecastError: null
}

const reducer = (state, action) => {
    switch (action.type) {
      case SET_LOCATION:
        return {
          ...state,
          initialLocation: action.payload
        };
      case SET_FORECAST:
        return {
          ...state,
          calendarData: action.payload.data,
          loadedDataLocation: action.payload.location,
          isLoadingForecast: false,
          forecastError: null
        };
      case SET_PLACES:
        return {
          ...state,
          places: action.payload
        };
      case SET_SELECTED_FORECAST: 
        return {
          ...state,
          selectedForecast: action.payload
        }
      case SET_LOADING_FORECAST: {
        return {
          ...state,
          isLoadingForecast: true
        }
      }
      case SET_FORECAST_ERROR_TYPE: {
        return {
          ...state,
          forecastError: action.payload
        }
      }
      default:
        throw new Error();
    }
}

export default reducer;
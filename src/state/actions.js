import {
    SET_LOCATION,
    SET_FORECAST,
    SET_PLACES,
    SET_SELECTED_FORECAST,
    SET_LOADING_FORECAST,
    SET_FORECAST_ERROR_TYPE
} from './reducer'

export const setLocation = (location) => ({
    type: SET_LOCATION,
    payload: location
})

export const setForecast = (data, location) => ({
    type: SET_FORECAST,
    payload: {
        data,
        location
    }
})

export const setPlaces = (places) => ({
    type: SET_PLACES,
    payload: places
})

export const setSelectedForecast = (rowIndex, columnIndex) => ({
    type: SET_SELECTED_FORECAST,
    payload: [rowIndex, columnIndex]
});

export const setLoadingForecast = () => ({
    type: SET_LOADING_FORECAST
})

export const setForecastErrorType = (errorType) => ({
    type: SET_FORECAST_ERROR_TYPE,
    payload: errorType
})

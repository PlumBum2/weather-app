import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import './theme.scss';
import './index.scss';
import { polyfillLoader } from 'polyfill-io-feature-detection';
// This function load polyfills only if needed. By default it uses polyfill.io
polyfillLoader({
  "features": "Promise,fetch,Array.prototype.includes,String.prototype.startsWith,Array.prototype.findIndex,Array.prototype.find",
  "onCompleted": main
});
function main() {
    var mountNode = document.getElementById("app");
    ReactDOM.render(<App/>, mountNode);
}

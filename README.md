weather-app

## Building and running on localhost
make sure node is installed on your system
```https://nodejs.org/en/

install dependencies:

```sh
npm install
```

To run in hot module reloading mode:

```sh
npm start
```

Open `http://localhost:1234` in your browser

To run unit tests:

```sh
npm run test
```

To create a production build:

```sh
npm run build-prod
```


## TODO NEXT:

* Unit tests coverage
* Define Components ProTypes and DefaultPropTypes
* Wind direction icon rotation according to parameter
* Details panel time replacement with "now" when most current time selected
* Details panel display weekday when not "Today"
* Initial location load according to ip might be wrong in some locations
* Pointer style on some elements
* Fav-icon
* Some minor layout missalignments with design
* Search suggestions markup for matching part of the search string
* Correct week day label format on mobile layout
* Refactor Input component to be more generic
* Mobile layout expand/collapse animation
* Mobile layout scroll to top of the page when time row selected
* Fix IE GLICHES: 
    a) layout issues on very small screen
    b) details panel border radius issue
    c) loader spinner not spinning
    d) dropdown suggestions top margin
    e) overall performance
    f) production build api header issues
* production build optimisation (size, obfuscation)



